FROM debian:bookworm-slim

# Install latexmk and texlive
RUN apt-get update && apt-get install -y --no-install-recommends \
    biber \
    latexmk \
    python3-pygments \
    texlive \
    texlive-bibtex-extra \
    texlive-binaries \
    texlive-fonts-extra \
    texlive-latex-extra \
    texlive-plain-generic \
    texlive-publishers \
    texlive-science \
    texlive-xetex \
 && rm -rf /var/lib/apt/lists/*
